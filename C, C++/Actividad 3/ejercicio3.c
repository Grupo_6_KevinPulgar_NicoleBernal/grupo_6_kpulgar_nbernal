#include <stdio.h>
#include <time.h>
#include <stdlib.h>

int *getRandom(){

	static int r[256];
	int i; 

	srand( (unsigned)time(NULL) );

	for (i = 0; i < 256; ++i){
		r[i] = rand() % 255;
		//printf("r[%d] = %d\n",i, r[i] );
	}

	return r;
}

int main(){

	int *p;
	int i,j;
	int bin[9];
	int num;

	p = getRandom();

	
	for ( i = 0; i < 256 ; i++){ //recorre matriz de numeros aleatorios

		num = *(p+i);

		printf ("r[%d]: %d\n",i,num); 

		for( j = 0; j < 9; j++){ //transforma a binario un decimal creando una matriz bin

			bin[j] = num % 2;
			num = num / 2;

			if(num == 0)
				break;
		}
		
		//imprimir por pantalla cada numero en binario
		int k;

		printf("bin: \n");

		for (k = j; k >= 0; k--){
			
			if(bin[k] == 1)
				printf ("%d LED ENCENDIDO\n",bin[k]);
			else
				printf ("%d LED APAGADO\n", bin[k]);

		}
		
	printf("\n");
		
	}

}