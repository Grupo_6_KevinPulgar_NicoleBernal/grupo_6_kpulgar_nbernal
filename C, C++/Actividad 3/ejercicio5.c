#include <stdio.h>
#include <math.h>

//Funcion para calcular promedio de un arreglo
float promedio ( int N, float arr[]){

	int j;
	float promedio;

	float suma = 0;


	for (j = 0; j < N; j++){

		suma = suma + arr[j];

	}

	promedio = suma / N;

	return promedio;
}

//funcion para calcular la varianza de un arreglo
float varianza ( int N, float arr[]){

	int j;
	float aux, varianza;

	float suma = 0;

	aux = promedio(N, arr);		//variable auxiliar para llamar a la funcion promedio

	for ( j = 0; j < N; j++){

		suma = suma + pow(arr[j] - aux, 2);  //calcula la suma acumulada
	}


		varianza = suma / (N - 1);

	return varianza;
}

float moda (int N, float arr[]){

	int i,j,aux;
	float num;
	
	
	for ( j = 0; j < N ; j++){

		aux = 0;

		for (i = 0; i < N; i++){

			if(arr[j] == arr[i]){
				
				aux = aux + 1;
			}

			if (aux >= 2){
				num = arr[j];
			}
		}

	}

	return num;

}

int main(){

	
	//------- INGRESO DE DATOS ------- //

	int cantidad, i;
	float PROM, VAR, DES, MODA;

	printf ("Ingrese cantidad de notas entre 1 y 100\n");
	scanf ("%d", &cantidad);
	

	float ALU[cantidad];	//declara arreglo unidimensional con la dimension de notas a ingresar por el usuario

	for ( i = 0; i < cantidad; i++){		//rellena arreglo con notas ingresadas por usuario

		printf("Ingrese nota n°%d\n",i+1);
		scanf("%f", &ALU[i]);
		
	}

	//promedio de notas
	PROM = promedio(cantidad, ALU);
	printf("El promedio de notas es: %.2f\n", PROM);

	//varianza de las notas
	VAR = varianza(cantidad, ALU);
	printf("La varianza de notas es: %.2f\n", VAR);

	//desviacion notas
	DES = sqrt(VAR);
	printf("La desviacion estandar de notas es: %.2f\n", DES);

	MODA = moda(cantidad, ALU);
	printf("La moda es: %.2f\n",MODA);


	return 0;

	



}