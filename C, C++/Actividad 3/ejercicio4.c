#include <stdio.h>
#include <time.h>
#include <stdlib.h>

//genera 24 temperaturas aleatorias que son guardadas en el arreglo TEM
int *getRandom(){

	static int TEM[24];
	int i; 

	srand( (unsigned)time(NULL) );

	for (i = 0; i < 24; ++i){

		TEM[i] = rand() % 50;
		printf("TEM%d = %d\n",i, TEM[i] );
	}

	return TEM;
}

//calcula el promedio de un arreglo
float promedio(int *array){

	double suma = 0;
	int i;

	for(i = 0; i < 24; i++){

		suma = suma + array[i];

	}

	return suma / 24;

}

//encuentra el valor maximo de un arreglo
int maximo (int *array){

	int max, hora;
	int j;

	for (j = 0; j < 24; j++){
		
		if(max < array[j]){

			max = array[j];
			hora = j;
		}
		

	}

	printf("La temperatura maxima en el día es %d °C a las %d hrs \n", max,hora);

	return 0;
}

//encuentra el valor minimo de un arreglo
int minimo (int *array){

	int min, hora;
	int k;

	for (k = 0; k < 24; k++){

		if(min > array[k]){

			min = array[k];
			hora = k;
		}
		
	}

	printf("La temperatura minima en el día es %d °C a las %d hrs\n", min,hora);

	return min;
}

int main(){

	int *p;
	float prom;
	int max,min;

	p = getRandom();

	//se obtiene el promedio de las temperaturas 
	prom = promedio(p);
	printf("la temperatura promedio del día es %f °C\n", prom);

	//temperatura máxima
	max = maximo (p);

	//temperatura minima
	min = minimo (p);

	
	
	return 0;
}