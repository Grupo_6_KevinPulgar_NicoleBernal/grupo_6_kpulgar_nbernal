#include <stdio.h> 

int main(){ 

	float S;
	float FA;
	float Tc; 
	
	printf("Ingresar sonidos de la chicharria ocurridos en 1 minuto: "); 
	scanf("%f", &S); 

	FA = S/4 + 40;  //relacion Fahrenheit sonido chicharra
	Tc = (FA-32)*5/9;  //transformacion Fahrenheit-Celsius
	
	printf("La temperatura equivalente en grados Fahrenheit es = %.2f °F", FA); 
	printf("\nLa temperatura equivalente en grados Celsius es = %.2f °C \n", Tc);

	return(0); 
}