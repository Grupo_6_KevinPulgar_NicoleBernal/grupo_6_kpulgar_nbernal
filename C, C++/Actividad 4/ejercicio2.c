#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int main(){

	char cad[50];
	char *mem_datos;
	int i;

	char *cantidad;
	char *caracter;
	int valor;

	printf("ingrese cadena de caracteres:\n");
	scanf("%s", cad);

	mem_datos = (char *) malloc(sizeof(char)*strlen(cad)); //crea espacio de memoria del tamaño del vector de caracteres

	// verifica la reserva espacio de memoria
	if(mem_datos == NULL) 	

		printf("Error al intentar reservar memoria\n");

	else {

		for (i = 0; i < strlen(cad); i++ ){ //guarda cada caracter en un espacio de memoria distinto

			
			mem_datos[i] = cad[i];
			
		}

		
		for (i=0; i < strlen(mem_datos); i++){	//recorre cadena de caracteres guardada en memoria

			cantidad = mem_datos + 2*i;			//puntero que indica el numero de veces que se repite el caracter
			caracter = cantidad + 1;			//puntero que indica el caracter a repetir

			valor = atoi(cantidad);				//transforma a un valor entero el caracter seleccionado


			while ( valor != NULL ){				//condicion para saber cuantas veces se debe imprimir el caracter

				printf("%c", *caracter);
				valor = valor - 1;
				
			}
		}
		
		printf("\n");
		free(mem_datos);	//libera memoria 
	}

return 0;	 

}

