#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int main() {
	char car[1];	// Variable del caracter a comparar
	char cad[50];	//Variable para los caracteres
	int i;
	int cont=0;		//Contador que indica el número de caracteres presentes iguales al ingresado
	printf("Ingrese 1 caracter que desea buscar:\n");//Ingreso del caracter a comparar
	scanf("%c",car);
	printf("Ingrese la cadena de caracteres (máx 50):\n");	//Ingreso de la cadena de caracteres
	scanf("%s",cad);

	for (i=0;i<strlen(cad);i++){		//Se realiza la comparación y se cuentan las coincidencias
		if (cad[i]==car[0])
			cont=cont+1;
	}
	printf("contador : %d\n", cont);	//Se imprime el total de coincidencias
	
	return 0;	
}
