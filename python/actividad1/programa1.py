import math

#ingreso de datos
alt = float(input("Ingrese altura del cono "))
radio = float(input("Ingrese radio del cono "))

#calculo area 

g = math.sqrt(pow(alt,2)+pow(radio,2))
area_lat = math.pi*radio*g

#resultado
print("El area lateral del cono es: ")
print(area_lat)
