import socket

HEADER = 64
PORT = 3074
SERVER = '158.251.91.68'
ADDR = (SERVER, PORT)
FORMAT = 'utf-8'
DISCONNECT_MESSAGE = 'DISCONNECT!'

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

client.connect(ADDR)

def codificar(msg):

    array_cod = []
   
    for i in msg:
    	array_cod.append(ord(i))

    string = (' '.join([str(elem) for elem in array_cod])) + " "
    return string
    
def send(msg):

	msg_cod = codificar(msg) 
	
	message = msg_cod.encode(FORMAT)

	msg_length = len(message)
	send_length = str(msg_length).encode(FORMAT)

	send_length += b' '*(HEADER-len(send_length))

	client.send(send_length)
	client.send(message)
	print(client.recv(2048).decode(FORMAT))



send('hello')
input()
send('asdasdf')
input()
send(DISCONNECT_MESSAGE)