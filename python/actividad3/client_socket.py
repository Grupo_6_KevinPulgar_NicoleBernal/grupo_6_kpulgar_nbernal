import socket

HEADER = 64
PORT = 3074
SERVER = '158.251.91.68'
MAC_ADDR = '09:b7:00:e6:08:9c'  # Mac Address fictisia para efectos de seguridad
ADDR = (SERVER, PORT)
FORMAT = 'utf-8'
DISCONNECT_MESSAGE = 'DISCONNECT!'

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

client.connect(ADDR)

def send(msg):
    message = msg.encode(FORMAT)

    msg_length = len(message)
    send_length = str(msg_length).encode(FORMAT)

    send_length += b' '*(HEADER-len(send_length))

    client.send(send_length)
    client.send(message)
    print(client.recv(2048).decode(FORMAT))

send(MAC_ADDR)	# al momento de establecer la conexión entre servidor-cliente, este envía inmediatamente su Mac
input()
send('Hola Mundo') #mensaje a enviar luego de haber realizado el handshake con el servidor
input()
send(DISCONNECT_MESSAGE)
