import socket
import threading

HEADER = 64
PORT = 3074
SERVER = '158.251.91.68'
ADDR = (SERVER, PORT)
FORMAT = 'utf-8'
DISCONNECT_MESSAGE = 'DISCONNECT!'
ALLOWED_MAC = '09:b7:00:e6:08:9c'  #registro de Mac Address que pueden establecer conexión con el servidor
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

server.bind(ADDR)

def handle_client(conn, addr):
    
    print(f"[NEW CONNECTION] {addr} connected.")

    connected = True

    msg_length = conn.recv(HEADER).decode(FORMAT)

    if msg_length:
        
        msg_length = int(msg_length)
        msg = conn.recv(msg_length).decode(FORMAT)

        if msg != ALLOWED_MAC:  #sentencia para verificar si Mac Address enviada por el cliente esta en el registro del servidor

            print(f"[HANDSHAKE] {msg} mac address is not allowed") # Mac Address del cliente no se encuentra en los registros del servidor
            connected = False                                      # por lo tanto no se puede establecer la conexión cliente-servidor
            conn.send("Handshake FAILED".encode(FORMAT))

        else:                                                      # Mac Address del cliente se encuentra en los registros del servidor

            print(f"[HANDSHAKE] {msg} mac address is allowed")     # por lo tanto la conexión cliente-servidor es posible(se realiza Handshake) y se puede haber transferencia de datos
            conn.send("Handshake SUCESFULL".encode(FORMAT))

    while connected:                                               # Dado que el Handshake fue exitoso, la conexión se mantiene. 
        msg_length = conn.recv(HEADER).decode(FORMAT)
        if msg_length:
            msg_length = int(msg_length)
            msg = conn.recv(msg_length).decode(FORMAT)

            if msg == DISCONNECT_MESSAGE:
                connected = False

            print(f"[{addr}] {msg}")
            conn.send("Msg received".encode(FORMAT))
    

                

    conn.close()


def start():
    server.listen()
    print(f"[LISTEN] Server is listening on address {ADDR}")
    while True:
        conn, addr = server.accept()
        thread = threading.Thread(target=handle_client, args=(conn, addr))
        thread.start()
        print(f"[ACTIVE CONNECTIONS] {threading.activeCount() - 1}")


print("[STARTING] server is running.....")
start()
#mac()
