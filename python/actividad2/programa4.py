num = int(input('Ingrese valor final '))
sum = 0

for i in range(1,num+1):  #sentencia para realizar la cuenta regresiva de cada dígito hasta llegar al numero indicado por el usuario 

	sum = sum + i 		  #operación que realiza la suma de cada dígito

	if i < num:				
		print(i ,'+',end=' ')
	else: 
		print(i,end=' ')


print('=',sum)	#resultado de la suma de cada dígito