# PROGRAMA UTILIZANDO CICLO WHILE
num1 = 2

while num1 <= 14:			#sentencia para realizar la cuenta creciente
	print(num1)
	num1 = num1 + 2

print('Adios!')

#----------------------------------#
print('\n')
#----------------------------------#

#PROGRAMA UTILIZANDO EL CICLO FOR
print('Hello')

for i in range(10,1,-2):	#sentencia para realizar la cuenta decreciente
	print(i)
