
varA = 'hola' 
varB = 'auto'

if isinstance(varA,str) or isinstance(varB,str) :	#sentencia para reconocer si una de las variables es un string
	print ('String Involucrado')
else:												#en el caso de no ser así se procede a comparar ambas variables
	if varA > varB:
		print ('Mas Grande')
	elif varA == varB:
		print ('Igual')
	else:
		print ('Mas Pequeño')
